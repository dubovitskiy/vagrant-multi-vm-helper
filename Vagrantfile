# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'json'

ENV['LC_ALL'] = 'en_US.UTF-8'

# Search cluster file in current directory by default
default_machines_filename = "#{__dir__}/definition.json"

# Cluster file path can be specified via CLUSTER_FILENAME env
machines_filename = ENV['DEFINITION_FILENAME'] || default_machines_filename

begin
  machines_file = File.read(machines_filename)
rescue IOError
  puts "File \"#{machines_filename}\" does not exist"
  exit 1
end

begin
  cluster_definition = JSON.parse(machines_file)
rescue JSONError
  puts "File \"#{machines_filename}\" doesn't contain a valid JSON"
  exit 1
end

puts "Using #{machines_filename} cluster definition"

machines = cluster_definition.fetch('machines', [])

# If no hostname present, it will be generated as "#{host-prefix}-<integer>"
host_prefix = cluster_definition.fetch('host_prefix', 'node')

default_box = cluster_definition['default_box']
default_cpus = cluster_definition.fetch('default_cpus', 1)
default_ram = cluster_definition.fetch('default_ram', 512)

def symbolize_keys(hash)
  symbolised_options = {}
  hash.map do |key, value|
    symbolized_key = key.to_sym
    symbolised_options[symbolized_key] = value
  end
  symbolised_options
end


Vagrant.configure('2') do |config|
  # Increases when hostname is generated
  host_counter = 0

  machines_list = []

  if machines.is_a?(Array)
    # Case when machines declared explicitly
    # as an array of objects with parameters
    machines_list = machines.dup
  elsif machines.is_a?(Integer)
    # Case when machines declared as integers and
    # supposed to be created with default values
    if default_box.nil?
      puts '"default_box" is required parameter'
      exit 1
    end
    machines.times do
      host_counter += 1
      machine = {
        hostname: "#{host_prefix}-#{host_counter}",
        box: default_box,
        hardware: {
          ram: default_ram,
          cpus: default_cpus
        }
      }
      machines_list.push(machine)
    end
  end

  machines_list.each do |machine|
    machine_hostname = machine.fetch('hostname', nil)
    # Increase counter and generate a hostname
    if machine_hostname.nil?
      host_counter += 1
      machine_hostname = "#{host_prefix}-#{host_counter}"
    end

    machine_box = machine.fetch('box', default_box)

    if machine_box.nil?
      puts "\"default_box\" is not specified. Cannot create machine #{machine}"
      exit 1
    end

    machine_hardware = machine.fetch('hardware', {})

    machine_ram = machine_hardware.fetch('ram', default_ram)
    machine_cpus = machine_hardware.fetch('cpus', default_cpus)

    config.vm.define machine_hostname do |machine_config|
      machine_config.vm.box = machine_box

      # Some boxes don't have an ability to set a
      # hostname (like alpine)
      machine_config.vm.hostname = machine_hostname

      # Set hardware parameters
      machine_config.vm.provider 'virtualbox' do |virtualbox|
        virtualbox.memory = machine_ram
        virtualbox.cpus = machine_cpus
      end

      provision = machine.fetch('provision', [])

      provision.each do |shell_command|
        machine_config.vm.provision :shell, inline: shell_command
      end

      machine_network = machine['network']

      # Set network config
      unless machine_network.nil?
        vagrant_network_type = machine_network['network_type']
        network_type = "#{vagrant_network_type}_network"
        network_options = symbolize_keys(machine_network['options'])
        config.vm.network network_type, **network_options
      end
    end
  end
end
